# Cindr's Core Extensions

## Philosophy and Explanation

- At this point, I just keep on iterating and making sure that I would keep on having the least number of extensions as much as possible, as I feel like it is similar to the browser extensions problem that the more extensions you have on your browser, the less performant it would be.

- This might be my second attempt at trying to declutter my extensions list, and I just really need to control myself to what I need over to what I want.

- The core extensions I should list here are the most essential ones necessary for just basic editing and such, and should be close to the vanilla experience without any support for most languages, like JS/TS or HTML, as if you are just using this as basic notepad.

- I would separate to different other extension packs depending on the requirement of the project (e.g. a Vue Project or a Nuxt 3 Project should have Volar; a Go Project should have Go), and that they are separated into different workspace-only extensions for easy setup.

- I might need to sacrifice a bit of convenience when working on just small files with VS Code, but I think I have [Lapce](https://lapce.dev) to do the job for just basic text editing over something like Notepad++ or Sublime..

- Anyways, I have trimmed down most of the "bloat" to these core VS Code Extensions for basic editing and probably some small HTML projects.

## Extensions (Subject to Change)

> Might probably add or remove some of these extensions.

### Theme

1. [Catppuccin](https://marketplace.visualstudio.com/items?itemName=Catppuccin.catppuccin-vsc)
    - My primary theme for all my IDEs. I seem on changing this a lot due to the fact that I'm indecisive of what I want it to look like, but now I'm committing to a single theme for all as part of my New Year's Resolution or something like that.

2. [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
    - My primary icon theme. Ditto to the previous item. Also, been using this for a long time, so you could never go wrong with it at this point.

### Core Essentials

1. [Vim](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim)
   - I can never live without vim these days, so I use this vim extension for VS Code to satisfy my vim addiction.

2. [Sublime Keybindings](https://marketplace.visualstudio.com/items?itemName=ms-vscode.sublime-keybindings)
   - This is just addiitonal keybindings (particularly the multiple cursor) since I've also grown to get used to it too.

3. [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
   - I <3 Markdown, and I use vanilla markdown for taking notes, writing todo lists, etc. So this is just a plain necessity for me to have.

4. [TODO Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggley.todo-tree) & [TODO Highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)
    - I like a way for me to monitor my TODOs, so I just have these two handy around.

5. [TabNine](https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode)
   - `TabNine > Github Copilot`. Change my mind.

6. [Hex Editor](https://marketplace.visualstudio.com/items?itemName=ms-vscode.hexeditor)
   - You never know when I'll use this, so I just keep it handy.

7. [Registry IntelliCode Support](https://marketplace.visualstudio.com/items?itemName=ionutvmi.reg)
   - Usefull for editing registry scripts on windows.

8. [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
   - Usefull for knowing git status and last git commit and who made that last commit kind of stuff.

9. [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
    - Usefull for relative or absolute path mapping and such.

10. [Indent Rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)
    - Usefull for proper indentation for languages that may or may not relay on intentation (eg. Python or YAML).

11. [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwichdey.LiveServer)
    - Usefull for small HTML project testing without `package.json`.
  
12. [REST Client](https://marketplace.visualstudio.com/items`?itemName=human`.rest-client)
    - A simple REST client for multiple project types.

13. [Remote WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)
    - For Remote or WSL Workspaces.

14. [Live Share](https://marketplace.visualstudio.com/items?itemName=ms-vsliveshare.vsliveshare) & [Live Share Audio](https://marketplace.visualstudio.com/items?itemName=ms-vsliveshare.vsliveshare-audio)
    - For Live Share use & collaboration in mind.

15. [Discord Rich Presence](https://marketplace.visualstudio.com/items?itemName=icrawl.discord-vscode)
    - Discord Rich Presence support.

16. [Firefox Debugger](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug)
    - For Firefox debugging in tandem with Live Server. 